// ==UserScript==
// @name     WebTelegram Theme Engine
// @description Create and share themes for web.telegram.org
// @icon     https://web.telegram.org/favicon.ico
// @version  1
// @grant    none
// @run-at   document-idle
// @include  https://web.telegram.org/*
// ==/UserScript==

//context wrapper 
(()=>{
  
// BLOCK utilities
//string x to be expanded to length n by prepending characters c
let lead = (x,n,c)=>{
  while (x.length < n) x = c+x;
  return x;
}
let color2hex = (color)=>{
  if (color.startsWith('rgb')) {
    //transform to hex, format is rgba?(i,i,i,f)
    parts = 
      color.substr(color.indexOf('(')+1, color.lastIndexOf(')')). //extract numbers within brackets
      split(','); //split on comma
    for (c = 0; c < 3; c++) //convert color ints 0-255
      parts[c] = lead(parseInt(parts[c]).toString(16), 2, '0');
    if (parts.length > 3) //convert optional alpha 0.0-1.0, cramp into one byte 0-255 for #aarrggbb representation (precision is lost!)
      parts[3] = lead(Math.floor((parseFloat(parts[3])*255)).toString(16), 2, '0');
    return parts.length>3 ?
      '#'+parts[3]+parts[0]+parts[1]+parts[2] : //return ARGB code
      '#'+parts[0]+parts[1]+parts[2]; //return RGB code
  } else if (color.startsWith('#')) {
    return color;
  } else
    throw 'Illegal color '+color;
}
let hex2color = (code)=>{
  //expand color to rgba if len > 7 (#11223344)
  if (code.startsWith('#') && code.length > 7) {
    v = []; // [ r, g, b, a ]
    parts = []; // [ AA, RR, GG, BB ]
    code = code.substr(1);
    while (code.length > 1) {
      parts.push( parseInt(code.substr(0,2), 16) );
      code = code.substr(2);
    }
    for (c = 1; c < 4; c++) {
      v.push(parts[c]); //push rgb colors
    }
    v.push( parts[0] / 255.0 ); //push alpha convert back to 0.0-1.0 from byte value 0-255
    return 'rgba('+v.join(',')+')';
  } else {
    return code; //either #rrggbb or rgb(r,g,b) - both parsable by browser
  }
}  

/** https://gist.github.com/mjackson/5311256
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSL representation
 */
function rgbToHsl(r, g, b) {
  r /= 255, g /= 255, b /= 255;

  var max = Math.max(r, g, b), min = Math.min(r, g, b);
  var h, s, l = (max + min) / 2;

  if (max == min) {
    h = s = 0; // achromatic
  } else {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

    switch (max) {
      case r: h = (g - b) / d + (g < b ? 6 : 0); break;
      case g: h = (b - r) / d + 2; break;
      case b: h = (r - g) / d + 4; break;
    }

    h /= 6;
  }

  return [ h, s, l ];
}

/** https://gist.github.com/mjackson/5311256
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  l       The lightness
 * @return  Array           The RGB representation
 */
function hslToRgb(h, s, l) {
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1/6) return p + (q - p) * 6 * t;
      if (t < 1/2) return q;
      if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;

    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }

  return [ r * 255, g * 255, b * 255 ];
}

/* https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript */
function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}
//BLOCK
  
// BLOCK apply css color changes
//theme color map
let theme = {
};
let themeTemplate = {};
let doProcTemplateTheme = true;

let procDocument = ()=>{
  for (i = 0; i < document.styleSheets.length; i++) {
    procSheet(document.styleSheets[i]);
  }
  if (doProcTemplateTheme) {
    doProcTemplateTheme = false;
    //reduce color map
    templateColorSet = new Set(Object.keys(themeTemplate));
    themeTemplate = {};
    for (color of templateColorSet) {
      themeTemplate[color] = color;
    }
    //unsafeWindow.themeTemplate = themeTemplate; //will be protected
    sessionStorage.setItem('themeTemplate', JSON.stringify(themeTemplate));
  }
  if (localStorage.getItem('theme')==null) {//did not run before / no theme set
    Object.keys(themeTemplate).forEach(k=> //fill the theme object manually for the theme editor to work
			theme[k]=themeTemplate[k]
		);
  }
}
let procSheet = (sheet)=>{
  console.log(sheet.href, sheet);
  re = [];
  for (j = 0; j < sheet.cssRules.length; j++) {
    rule = sheet.cssRules[j];
    selector = rule.selectorText;
    re = procStyles(rule);
    if (re.length > 0) {
      //update rule:
      sheet.deleteRule(j);
      sheet.insertRule(selector + '{' + re.join('') + '}', j);
    }
  }
}
/** has to return a copy since rules are immutable */
let procStyles = (styles)=>{
  if (!styles.style) return [];
  restyle = [];
  for (k = 0; k < styles.style.length; k++) {
    restyle.push( procRule(styles.style[k], styles.style[styles.style[k]]) );
  }
  return restyle;
}
/** return a new key->value string to be inserted into the mutated copy in procStyles */
let procRule = (key, value)=>{
  cols = value.match(/(#[0-9A-Fa-f]{6})|(rgba?\([0-9]+(, [0-9]+)+\))/g);
  if (cols && cols.length>0) {
    for (col of cols) {
      hcol = procColor(col);
      if (hcol != col) {
        value = value.replace(col, hcol);
      }
    }
  }
  return `${key}: ${value};`;
}
let procColor = (color)=>{
  unicolor = color2hex(color);
  if (doProcTemplateTheme) {
    themeTemplate[unicolor] = unicolor;
  }
  if (typeof(theme[unicolor]) !== 'undefined') {
    return hex2color(theme[unicolor]);
  } else {
    return color;
  }
}
// END BLOCK apply css color changes

// BLOCK inject theme menu
let injectMenu = ()=>{
  funToggleMenu = ()=>{
    document.querySelectorAll("div.ThemeMenu").
    forEach(x=>
            x.style.display = ( x.style.display == 'block' ?
                               'none' :
                               'block' )
           );
  };
  
  //dropdown entry
  {
    ul = document.querySelector('ul.dropdown-menu');
    li = document.createElement('li');
    a = document.createElement('a');
    a.addEventListener('click', funToggleMenu);
    a.innerText = 'ThemeEngine Menu';
    li.appendChild(a);
    ul.appendChild(li);
  }
  
  //create menu
  {
    //backdrop
    glassPane = document.createElement('div');
    glassPane.style.position = 'fixed';
    glassPane.style.display = 'none';
    glassPane.classList.add('ThemeMenu');
    glassPane.classList.add('modal-backdrop');
    glassPane.classList.add('in'); //opacity .5
    document.body.appendChild(glassPane);
  }
  {
    //content
    clickAway = document.createElement('div');
    clickAway.addEventListener('click', funToggleMenu);
    clickAway.classList.add('ThemeMenu');
    clickAway.classList.add('modal');
    clickAway.classList.add('in');
    document.body.appendChild(clickAway);
    
    dlg = document.createElement('div');
    dlg.classList.add('modal-dialog');
    clickAway.appendChild(dlg);
    
    cont = document.createElement('div');
    cont.classList.add('modal-content');
    cont.style.margin = '0 auto';
    cont.style.marginTop = '50vh';
    cont.style.transform = 'translateY(-50%)';
    cont.style.minHeight = '320px';
    cont.style.maxWidth = '640px';
    cont.addEventListener('click', (event)=>{ //preven clickAway on children
      event.stopPropagation();
      return false;
    });
    dlg.appendChild(cont);
    
    //head
    {
      head = document.createElement('div');
      head.classList.add('md_modal_head');
      cont.appendChild(head);
      
      titleWrap = document.createElement('div');
      titleWrap.classList.add('md_modal_title_wrap');
      head.appendChild(titleWrap);
      
      actions = document.createElement('div');
      actions.classList.add('md_modal_actions_wrap');
      actions.classList.add('clearfix');
      titleWrap.appendChild(actions);
      
      close = document.createElement('a');
      close.classList.add('md_modal_action');
      close.classList.add('md_modal_action_close');
      close.addEventListener('click', funToggleMenu);
      close.innerText = 'Close';
      actions.appendChild(close);
      
      title = document.createElement('div');
      title.classList.add('md_modal_title');
      title.innerText = 'ThemeEngine Menu';
      titleWrap.appendChild(title);
    }
    
    //body
    {
    	wrapper = document.createElement('div');
      wrapper.classList.add('md_modal_body');
      cont.appendChild(wrapper);
      
      scrollpane = document.createElement('div');
      scrollpane.style.maxHeight = '220px';
      scrollpane.style.overflow = 'auto';
      wrapper.appendChild(scrollpane);
      
      table = document.createElement('table');
      table.style.width = '50%';
      table.style.margin = '0 auto';
      table.style.textShadow = '1px 1px 1px #000, -1px -1px 1px #000, -1px 1px 1px #000, 1px -1px 1px #000';
      table.style.color = '#fff';
      table.style.fontFamily = 'Courier';
      scrollpane.appendChild(table);
      
      tr = document.createElement('tr');
      table.appendChild(tr);
      
      td = document.createElement('th');
      td.innerText = "Default";
      td.style.width = '33%';
      tr.appendChild(td);
      
      td = document.createElement('th');
      td.innerText = "Active";
      td.style.width = '33%';
      tr.appendChild(td);
      
      td = document.createElement('th');
      td.style.width = '33%';
      tr.appendChild(td);
      
      button = document.createElement('input');
      button.setAttribute('type', 'button');
      button.setAttribute('value', 'Reset All');
      button.addEventListener('click', ()=>{
        console.log(table);
      	table.querySelectorAll('input[type=color]').forEach(p=>p.value=hex2color(p.name))
      });
      button.style.width = '100%';
      button.style.background = 'none';
      button.style.borderRadius = '.3em';
      td.appendChild(button);
      
      Object.keys(theme).forEach(k=>{
      	tr = document.createElement('tr');
        table.appendChild(tr);
        
        td = document.createElement('td');
      	td.style.background=hex2color(k);
        td.innerText = k;
      	tr.appendChild(td);
      
        td = document.createElement('td');
        tr.appendChild(td);
        
        picker = document.createElement('input');
        picker.style.border = 'none';
    		picker.style.padding = '0';
    		picker.style.width = '100%';
        picker.setAttribute('type', 'color');
        picker.setAttribute('name', k);
        picker.setAttribute('value', hex2color(theme[k]));
        td.appendChild(picker);
        
        td = document.createElement('td');
        tr.appendChild(td);
        
        button = document.createElement('input');
        button.setAttribute('type', 'button');
        button.setAttribute('name', k);
        button.setAttribute('value', 'Reset');
        button.addEventListener('click', (event)=>{
          p = table.querySelector('input[type=color][name="'+k+'"]');
          console.log(p);
        	p.value = hex2color(p.name);
        });
        button.style.width = '100%';
        button.style.background = 'none';
    		button.style.borderRadius = '.3em';
        td.appendChild(button);
      });
      
      clear = document.createElement('div');
      clear.style.padding = '10px';
      wrapper.appendChild(clear);
      
      function createThemeObject() {
        var newTheme = {};
        table.querySelectorAll('input[type=color]').forEach(p=>{
					if (typeof(theme[p.name])!=='undefined'){ //prevent value injection by html edits
          	//fetch alpha prefix from name
            var alpha = (p.name.length > 7) ? p.name.substr(1,2) : '';
            //slice alpha into value
            var val = color2hex(p.value)
            val = val.slice(0,1)+alpha+val.slice(1)
            if (p.name.length>7)
              console.log(color2hex(p.value), '->', val);
            newTheme[p.name] = val;
          }
        });
        return newTheme;
      }
      
      button = document.createElement('a');
      button.innerText = 'Default Dark';
      button.style.display = 'inline-block';
      button.style.width = '20%';
      button.style.textAlign = 'center';
      button.addEventListener('click', ()=>{
        darkTheme = {};
        Object.keys(JSON.parse(sessionStorage.getItem('themeTemplate'))).forEach(color=>{
          cs = color.length > 7 ? color.substr(3) : color.substr(1);
          pre = color.length > 7 ? color.substr(0,3) : color.substr(0,1);

          r = parseInt(cs.substr(0,2), 16); cs = cs.substr(2);
          g = parseInt(cs.substr(0,2), 16); cs = cs.substr(2);
          b = parseInt(cs.substr(0,2), 16);

          hsl = rgbToHsl(r,g,b);
          rgb = hslToRgb(hsl[0], hsl[1], 1-hsl[2]).map(x=>Math.floor(x).toString(16)).map(x=>x.length==1?'0'+x:x);
          cs = pre+rgb.join('');

          darkTheme[color] = cs;
        })
        localStorage.setItem('theme', JSON.stringify(darkTheme));
        window.location.reload();
      });
      clear.appendChild(button);
      
      button = document.createElement('a');
      button.innerText = 'Restore Default';
      button.style.display = 'inline-block';
      button.style.width = '20%';
      button.style.textAlign = 'center';
      button.addEventListener('click', ()=>{
        localStorage.removeItem('theme');
        window.location.reload();
      });
      clear.appendChild(button);
      
      button = document.createElement('a');
      button.innerText = 'Share to Clipboard';
      button.style.display = 'inline-block';
      button.style.width = '20%';
      button.style.textAlign = 'center';
      button.addEventListener('click', ()=>{
        copyTextToClipboard('WebTelegramTheme:'+JSON.stringify(createThemeObject()));
        alert('Theme copied!');
      });
      clear.appendChild(button);
      
      button = document.createElement('a');
      button.innerText = 'Import from Clipboard';
      button.style.display = 'inline-block';
      button.style.width = '20%';
      button.style.textAlign = 'center';
      button.addEventListener('click', ()=>{
        var raw = prompt('Insert Theme here:');
        console.log(raw.substr(0,20));
        if (!raw.startsWith('WebTelegramTheme:')) {
          alert('Invalid Theme');
      	} else {
          var tmpTheme = {};
          try {
            raw = JSON.parse(raw.substr(17));
            Object.keys(raw).forEach(k=>{
              if (typeof(theme[k])==='undefined') 
                throw "Invalid color: "+k;
              tmpTheme[k] = raw[k];
            });
            console.log(tmpTheme);
            localStorage.setItem('theme', JSON.stringify(tmpTheme));
            window.location.reload();
          } catch (err) {
            alert('Import failed');
            console.log(err);
          }
        }
      });
      clear.appendChild(button);
      
      save = document.createElement('a');
      save.innerText = 'Save Theme';
      save.style.display = 'inline-block';
      save.style.width = '20%';
      save.style.textAlign = 'center';
      save.addEventListener('click', ()=>{
        localStorage.setItem('theme', JSON.stringify(createThemeObject()));
        window.location.reload();
      });
      clear.appendChild(save);
    }
  }
}
// END BLOCK inject theme menu

// BLOCK actually perform changes
  console.log('Load Event Triggered');
  theme = JSON.parse(localStorage.getItem('theme'));
  if (theme == null) theme = {}; //let theme always be an object
  console.log('Local storage loaded:', theme);
  window.setTimeout( () => {
    procDocument();
    console.log('Styles parsed');
    injectMenu();
  }, 1000);
// END BLOCK actually perform changes

})(); //auto execute